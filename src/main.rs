//use core::sync::atomic::AtomicPtr;
use std::thread::spawn;

fn main() {
    println!(
        "Testing how many high bits we can set until the pointer references the wrong address"
    );
    println!("!!! This is expeced to SEGFAULT at some point!!!!");

    /*
    let mut obj = Box::new(MyAlignedObject::new());
    println!("\n\n    looping counter back");
    for n in 1..=4 {
        let r = test_tag(&mut obj, n);
        if r.is_err() {
            //break;
        }
    }
    */
    let tf = spawn(|| {
        let mut obj = Box::new(MyAlignedObject::new());
        println!("\n\n    looping back");
        for n in 1..=16 {
            let r = test_back(&mut obj, n);
            if r.is_err() {
                //break;
            }
        }
    });
    let _ = tf.join();

    let tb = spawn(|| {
        let mut obj = Box::new(MyAlignedObject::new());
        println!("\n\n    looping front");
        for n in 1..=16 {
            let r = test_front(&mut obj, n);
            if r.is_err() {
                break;
            }
        }
    });
    let _ = tb.join();
}

fn test_back(obj: &mut MyAlignedObject, n: usize) -> Result<(), ()> {
    // all ones
    let ones = !0usize;
    let (mut tag, _overflow) = ones.overflowing_shr(64 - n as u32);
    if n == 0 {
        tag = 0;
    }

    test_tag(obj, tag)
}
fn test_front(obj: &mut MyAlignedObject, n: usize) -> Result<(), ()> {
    // all ones
    let ones = !0usize;
    let (mut tag, _overflow) = ones.overflowing_shl(64 - n as u32);
    if n == 0 {
        tag = 0;
    }

    test_tag(obj, tag)
}

fn test_tag(obj: &mut MyAlignedObject, tag: usize) -> Result<(), ()> {
    let pointer = obj as *mut MyAlignedObject;

    let tagged_ptr = ((pointer as usize) | tag) as *mut MyAlignedObject;
    println!("\ndereferencing with 0x{:16x}", tag);
    flush();
    println!("          address: {:18p}", tagged_ptr);
    flush();

    println!("  ==> DBG <== {:?}", unsafe { &(*tagged_ptr) });
    let fifth = unsafe { (*tagged_ptr).data[5] };
    if obj.data[5] == fifth {
        Ok(())
    } else {
        eprint!(
            " --> fifth element does not match! expected {}; foung {};",
            obj.data[5], fifth
        );
        flush();
        Err(())
    }
}

fn flush() {
    use std::io::Write;
    let r = std::io::stdout().flush();
    if let Err(e) = r {
        eprintln!("unable to flush: {:?}", e);
    }
    let r = std::io::stderr().flush();
    if let Err(e) = r {
        eprintln!("unable to flush: {:?}", e);
    }
}

#[derive(Debug)]
struct MyAlignedObject {
    name: String,
    data: [u8; 64],
}
impl MyAlignedObject {
    fn new() -> Self {
        let mut data = [0u8; 64];
        for (i, nth) in data.iter_mut().enumerate() {
            *nth = i as u8;
        }
        MyAlignedObject {
            data,
            name: "hello".to_string(),
        }
    }
}
